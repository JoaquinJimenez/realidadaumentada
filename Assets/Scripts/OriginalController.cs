using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginalController : MonoBehaviour
{
    public GameObject[] models;

    public void disable(){
        foreach(GameObject model in models){
            model.SetActive(false);

        }
    }
}
